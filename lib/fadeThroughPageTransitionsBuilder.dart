import 'package:flutter/material.dart';
import 'package:animations/animations.dart';

class AppFT1 extends StatefulWidget {
  @override
  AppFT1State createState() => AppFT1State();
}

class AppFT1State extends State<AppFT1> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        pageTransitionsTheme: PageTransitionsTheme(
          builders: {
            TargetPlatform.android: FadeThroughPageTransitionsBuilder(),
            TargetPlatform.iOS: FadeThroughPageTransitionsBuilder(),
          },
        ),
      ),
      routes: {
        '/': (BuildContext context) {
          return Container(
            color: Colors.lightBlueAccent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Text('PageTransitionsBuilder Sample',
                      style: TextStyle(fontSize: 18, color: Colors.black, decoration: TextDecoration.none),
                      textAlign: TextAlign.center,),
                  ),

                ),
                Center(
                  child: MaterialButton(
                    child: Image.asset('images/forestRiver.jpg'),
                    onPressed: () {
                      Navigator.of(context).pushNamed('/a');
                    },
                  ),
                ),
              ],
            ),
          );
        },
        '/a' : (BuildContext context) {
          return Container(
            color: Colors.purple,
            child: Center(
              child: MaterialButton(
                child: Image.asset('images/SunsetMountain.jpg'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
          );
        },
      },
    );
  }
}