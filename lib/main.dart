import 'package:flutter/material.dart';
import 'circularProgressBar.dart';
import 'linearProgressBar.dart';
import 'containerTransform.dart';
import 'Navigator&Routes.dart';
import 'fadeThroughPageTransitionsBuilder.dart';
import 'fadeThroughTransition.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes:{
      //  '/': (context) => HomeScreen(),
        '/second': (context) => PageX(pageNumber: 2,),
        '/third': (context) => PageX(pageNumber: 3,),
        '/fourth': (context) => Page4(),
      },
      home: DefaultTabController(
        length: 6,
        child: Scaffold(
          //backgroundColor: Color.fromRGBO(15, 15, 15, 1),
          appBar: AppBar(
            title: Text('LAB6 DEMO APP'),
            automaticallyImplyLeading: true,
            bottom: TabBar(
              indicatorColor: Colors.white,
              tabs: [
                Tab(text: 'Nav'),
                Tab(text: 'CT'),
                Tab(text: 'FTT'),
                Tab(text: 'FTPTB',),
                Tab(text: 'CPB'),
                Tab(text: 'LPB'),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              NavRoutes(),
              ct(),
              AppFT2(),
              AppFT1(),
              circularBar(),
              linearBar(),
            ],
          ),
        ),
      ),
    );
  }
}