import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ct extends StatefulWidget {
  const ct({Key key}) : super(key: key);

  @override
  ContainerTransformState createState() => ContainerTransformState();
}

class ContainerTransformState extends State<ct> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 20, bottom: 20),
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          OpenContainer(
            openBuilder: (BuildContext context, VoidCallback action) => ExampleOpenedContainer(),
            closedBuilder: (BuildContext context, VoidCallback action) => ExampleClosedContainer(),
            tappable: true,
          ),
          SizedBox(height: 20, width: 20,),
          Text('Duration', style: TextStyle(fontSize: 28),),
          OpenContainerBuilder(1),
          OpenContainerBuilder(3),
          OpenContainerBuilder(5),
        ],
      ),
      )
    );
  }
}

class ExampleOpenedContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: RaisedButton(
        child: Text('Close'),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
  }
}

class ExampleClosedContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 50,
      alignment: Alignment.center,
      child: Text('Open'),
    );
  }
}

OpenContainer OpenContainerBuilder(int duration) {
  return
    OpenContainer(
      transitionDuration: Duration(seconds: duration),
      openBuilder: (BuildContext context, VoidCallback action) => OpenedContainer(),
      closedBuilder: (BuildContext context, VoidCallback action) => ClosedContainer(),
      tappable: true,
    );
}

class ClosedContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: SizedBox(width: 100, height: 100,
                child: Icon(Icons.open_in_browser),),
              color: Colors.grey[300],
            ),
            Container(
              padding: EdgeInsets.all(8),
                child: Text('Closed container',),
            ),
          ],
        ),
      ),
    );
  }
}

class OpenedContainer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 8, bottom: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 2),
                    spreadRadius: 0,
                    blurRadius: 1,
                    color: Colors.black26,
                  )
                ],
                color: Colors.white,
              ),
              child: Row(
                children: [
                  IconButton(
                    icon: Icon(Icons.home_outlined),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ],
              ),
            ),
            Card(
              margin: EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    child: SizedBox(width: 200, height: 400,
                      child: Icon(Icons.android_rounded, size: 100,)),
                    color: Colors.grey[300],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Opened container\n'),
                        Text('In this example, the closed and opened containers are essentially the same. '
                            'The main difference here is the additional text. However, you can use container'
                            ' transform to transition between any style of container widgets, containing'
                            ' all types of components.'),
                      ]
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class ActionContainerOpen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        child: Text('close'),
        onPressed: () => Navigator.of(context).pop(),
      )
    );
  }
}