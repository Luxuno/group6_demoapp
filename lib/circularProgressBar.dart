import 'package:flutter/material.dart';
import 'dart:math';

var rng = new Random();

void startedCode() {
  Container(
    height: 100, //Determines the height of the progress bar
    width: 100, //Determines the width of the progress bar
    child: CircularProgressIndicator(
      backgroundColor: Colors.transparent, //Color of background behind progress bar
      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue), //Color of progress bar
      value: 1, //Defined value of what progress bar is at (Remove this for indefinite)
      strokeWidth: 5, //Determines the size of the circle's line for the progress bar
    ),
  );
}

class circularBar extends StatefulWidget {
  @override
  _circularBarState createState() => _circularBarState();
}

class _circularBarState extends State<circularBar> {
  double sizeVal = 25;
  Color definedCol = Colors.transparent;
  Color definedCol2 = Colors.blue;
  double num1 = 0;
  List<bool> check = [false, false, false, false, false];
  List<String> HA = ['Nope', 'Nada', 'Stop pls', 'Nothing here'];
  List<Color> COLORS = [
    Colors.redAccent,
    Colors.lime,
    Colors.black,
    Colors.yellowAccent,
    Colors.purple,
    Colors.orangeAccent
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        genericCard1(),
      ],
    );
  }

  Container genericCard1() {
    return Container(
      height: 325,
      child: ListView(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        children: [
          //ITEM 1
          Column(
            children: [
              Container(
                  width: 200,
                  height: 290,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Indeterminate Progress'),
                        SizedBox(
                          width: 80,
                          height: 80,
                          child: CircularProgressIndicator(),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              final showSnackBar = SnackBar(
                                content: Text(HA[rng.nextInt(HA.length)]),
                                duration: const Duration(
                                    seconds: 1, milliseconds: 500),
                                backgroundColor: Colors.black,
                                behavior: SnackBarBehavior.floating,
                              );

                              Scaffold.of(context).showSnackBar(showSnackBar);
                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          //ITEM 2
          Column(
            children: [
              Container(
                  width: 200,
                  height: 290,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Changing strokeWidth'),
                        SizedBox(
                          width: 80,
                          height: 80,
                          child: CircularProgressIndicator(
                            strokeWidth: sizeVal,
                          ),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              sizeVal = rng.nextInt(100).toDouble();
                              print(check);
                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          //ITEM 3
          Column(
            children: [
              Container(
                  width: 200,
                  height: 290,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Background Color'),
                        SizedBox(
                          width: 80,
                          height: 80,
                          child: CircularProgressIndicator(
                            strokeWidth: 10,
                            backgroundColor: definedCol,
                          ),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              int indx = rng.nextInt(COLORS.length);
                              definedCol = COLORS[indx];
                              print(indx);
                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          //ITEM 4
          Column(
            children: [
              Container(
                  width: 200,
                  height: 290,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Changing valueColor'),
                        SizedBox(
                          width: 90,
                          height: 90,
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(definedCol2),
                            strokeWidth: 8,
                            backgroundColor:
                                check[3] ? definedCol : Colors.transparent,
                          ),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              int indx = rng.nextInt(COLORS.length);
                              definedCol2 = COLORS[indx];

                              print(indx);
                            });
                          },
                        ),
                        RaisedButton(
                          child: Text('Fun Button 2'),
                          onPressed: () {
                            setState(() {
                              final showSnackBar = SnackBar(
                                content: Text('Too short! Long press!'),
                                duration: const Duration(
                                    seconds: 1, milliseconds: 500),
                                backgroundColor: Colors.black,
                                behavior: SnackBarBehavior.floating,
                              );

                              Scaffold.of(context).showSnackBar(showSnackBar);
                            });
                          },
                          onLongPress: () {
                            setState(() {
                              if (check[3])
                                check[3] = false;
                              else
                                check[3] = true;
                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          //ITEM 5
          Column(
            children: [
              Container(
                  width: 200,
                  height: 290,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          'Changing Percentage Value (Determinate)',
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          width: 80,
                          height: 80,
                          child: CircularProgressIndicator(
                            value: num1,
                            strokeWidth: 10,
                            valueColor: check[4]
                                ? AlwaysStoppedAnimation<Color>(Colors.amber)
                                : AlwaysStoppedAnimation<Color>(Colors.blue),
                          ),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              if (num1 + 0.05 <= 1)
                                num1 += 0.1;
                              else
                                num1 = 0;

                              if ((num1 + 0.05).toInt() == 1)
                                check[4] = true;
                              else
                                check[4] = false;

                              print(num1);
                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          Column(
            children: [
              Container(
                  width: 200,
                  height: 290,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          'Changing Size (Height)',
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          width: 80,
                          height: 175,
                          child: CircularProgressIndicator(
                              strokeWidth: 7,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Colors.redAccent)),
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          Column(
            children: [
              Container(
                  width: 200,
                  height: 290,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          'Changing Size (Width)',
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          width: 160,
                          height: 80,
                          child: CircularProgressIndicator(
                            strokeWidth: 7,
                            valueColor: AlwaysStoppedAnimation<Color>(
                                Colors.greenAccent),
                          ),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
