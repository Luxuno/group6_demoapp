import 'package:flutter/material.dart';

class NavRoutes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HomeScreen()
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
      ),
      body: Center(
        child: Column(
          children: [
            SimpleDialog(
              title: Text('Where you trying to go?'),
              children: [
                SimpleDialogOption(
                  onPressed:  () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageX(pageNumber: 2,)),
                    );
                  },
                  child: Text('Page 2'),
                ),
                SimpleDialogOption(
                  onPressed:  () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageX(pageNumber: 3,)),
                    );
                  },
                  child: Text('Page 3'),
                ),
                SimpleDialogOption(
                  onPressed:  () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Page4()),
                    );
                  },
                  child: Text('Page 4'),
                ),
              ],
            ),
            Text('We are on Page 1')
          ],
        ),
      ),
      bottomSheet: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          ElevatedButton(
            child: Text('Go to Page 2'),
            onPressed: () {
              // Navigate to the second screen using a named route.
              Navigator.pushNamed(context, '/second');
            },
          ),
        ],
      ),
    );
  }
}

class PageX extends StatelessWidget {
  int pageNumber;

  PageX({this.pageNumber,}) {}

  @override
  Widget build(BuildContext context) {
    int NextPage = pageNumber + 1;
    int PrevPage = pageNumber - 1;
    return Scaffold(
      appBar: AppBar(
        title: Text('Page $pageNumber'),
      ),
      body: Center(
        child: Text('We are on Page $pageNumber'),
      ),
      bottomSheet: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ElevatedButton(onPressed:  () {
            if(pageNumber == 2 ) {
              if (this.pageNumber == 3)
                Navigator.pop(context);

              else
                Navigator.popUntil(context, ModalRoute.withName('/'));
            }
            // Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()),);

            else if(pageNumber == 3)
              Navigator.pushNamed(context, '/second');
          },
              child: Text('Go back to Page $PrevPage')
          ),
          ElevatedButton(
              onPressed:  () {
                if(pageNumber == 2)
                  Navigator.pushNamed(context, '/third');

                else if(pageNumber == 3)
                  Navigator.pushNamed(context, '/fourth');
              },
              child: Text('Go to Page $NextPage ')
          ),
        ],
      ),
    );
  }
}

class Page4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 4'),
      ),
      body: Center(
        child: Text('We are on Page 4'),
      ),
      bottomSheet: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ElevatedButton(onPressed:  () {
            Navigator.pushNamed(context, '/third'); },
              child: Text('Go back to Page 3')
          ),
          ElevatedButton(
              onPressed:  () {
                Navigator.popUntil(context, ModalRoute.withName('/'));
              },
              child: Text('Go to Home Page')
          ),
        ],
      ),
    );
  }
}