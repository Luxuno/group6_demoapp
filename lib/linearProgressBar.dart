import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math';

var rng = new Random();

void startedCode() {
  Container(
    width: 100, //Determines the width of the progress bar
    child: LinearProgressIndicator(
      backgroundColor: Colors.transparent, //Color of background behind progress bar
      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue), //Color of progress bar
      value: 1, //Defined value of what progress bar is at (Remove this for indefinite)
        minHeight: 30 //Determines the height of the progress bar
    ),
  );
}

class linearBar extends StatefulWidget {
  @override
  _linearBarState createState() => _linearBarState();
}

class _linearBarState extends State<linearBar> {
  int currentVal = 100;
  int maxVal = 100;
  double currentDouble = 1;
  double sizeVal = 25;
  Color definedCol = Colors.transparent;
  Color definedCol2 = Colors.blue;
  double num1 = 0;
  List<bool> check = [false, false, false, false, false];
  List<String> HA = ['Nope', 'Nada', 'Stop pls', 'Nothing here'];
  List<Color> COLORS = [Colors.redAccent, Colors.lime, Colors.black, Colors.yellowAccent, Colors.purple, Colors.orangeAccent];
  double definedHeight = 100;
  double definedWidth = 100;

  @override
  Widget build(BuildContext context) {
    return ListView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
            children: [
              genericCard2(),
              genericCard3()
            ],
    );
  }

  Column genericCard2() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(5),
          //width: 225,
          //Color of the card and its appearance
          child: Card(
            //elevation: 8,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  decoration: BoxDecoration(border: Border.all()),
                  child: LinearProgressIndicator(
                    value: currentDouble,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.redAccent),
                    backgroundColor: Colors.black54,
                    minHeight: 20,
                  ),
                ),

                //Image asset
                GestureDetector(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Image.asset(
                      'images/battle1.png',
                      fit: BoxFit.fill,
                    ),
                  ),
                  onTap: () {
                    showDialog(
                        barrierColor: Color.fromRGBO(0, 0, 0, 0.94),
                        context: context,
                        builder: (BuildContext context) {
                          return GestureDetector(
                            child: Image.asset(
                              'images/battle1.png',
                              fit: BoxFit.fitWidth,
                            ),
                            onTap: () {
                              Navigator.pop(context);
                            },
                          );
                        });
                  },
                ),
                SizedBox(height: 10),
                //OP's comment
                Text(
                  'A rogue Slime has appeared!',
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10),
                //Contains
                Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ActionChip(
                        //shape: RoundedRectangleBorder(),
                        tooltip: 'Hit him with your Sword!',
                        shape: StadiumBorder(
                            side: BorderSide(color: Colors.white)),
                        backgroundColor: Colors.red,
                        label: Text(
                          'ATTACK',
                        ),
                        avatar: Icon(
                          Icons.cancel,
                        ),
                        onPressed: () {
                          setState(() {
                            currentVal -= 10;
                            currentDouble = (currentVal / maxVal);

                            if (currentDouble <= 0)
                              {
                                victory();
                                currentVal = maxVal;
                                currentDouble = 1;
                              }
                          });
                        }),
                    ActionChip(
                        //shape: RoundedRectangleBorder(),
                        tooltip: 'Hit him with magic!',
                        shape: StadiumBorder(
                            side: BorderSide(color: Colors.white)),
                        backgroundColor: Colors.blueAccent,
                        label: Text(
                          'MAGIC',
                        ),
                        avatar: Icon(
                          Icons.ac_unit,
                        ),
                        onPressed: () {
                          setState(() {
                            currentVal -= 20;
                            currentDouble = (currentVal / maxVal);

                            if (currentDouble <= 0)
                              {
                                victory();
                                currentVal = maxVal;
                                currentDouble = 1;
                              }
                          });
                        }),
                  ],
                ),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void victory() {
    showDialog(
        barrierColor: Color.fromRGBO(0, 0, 0, 0.94),
        context: context,
        builder: (BuildContext context) {
          return GestureDetector(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 100,
                  width: 200,
                  child: Card(
                    child: Column(mainAxisAlignment: MainAxisAlignment.center,children: [Text('You win! Press to go again.'),],),
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pop(context);
            },
          );
        });
  }

  Container genericCard3() {
    return Container(
        child: Column(
          //shrinkWrap: true,
        //scrollDirection: Axis.vertical,
        children: [
          //ITEM 1
          Column(
            children: [
              Container(
                height: 150,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Indeterminate Progress'),
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 40,
                          //height: 30,
                          child: LinearProgressIndicator(minHeight: 30),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              final showSnackBar = SnackBar(
                                content:
                                Text(HA[rng.nextInt(HA.length)]),
                                duration: const Duration(seconds: 1, milliseconds: 500),
                                backgroundColor:
                                Colors.black,
                                behavior: SnackBarBehavior.floating,
                              );

                              Scaffold.of(context).showSnackBar(showSnackBar);
                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          //ITEM 3
          Column(
            children: [
              Container(
                  height: 150,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Background Color'),
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 40,
                          //height: 30,
                          child: LinearProgressIndicator(
                            backgroundColor: definedCol,
                              minHeight: 30,
                          ),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              int indx = rng.nextInt(COLORS.length);
                              definedCol = COLORS[indx];
                              print(indx);
                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          //ITEM 4
          Column(
            children: [
              Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Changing valueColor'),
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 40,
                          //height: 30,
                          child: LinearProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color> (definedCol2),
                            backgroundColor: check[3] ? definedCol : Colors.transparent,
                            minHeight: 30,

                          ),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              int indx = rng.nextInt(COLORS.length);
                              definedCol2 = COLORS[indx];

                              print(indx);
                            });
                          },
                        ),
                        RaisedButton(
                          child: Text('Fun Button 2'),
                          onPressed: () {
                            setState(() {
                              final showSnackBar = SnackBar(
                                content:
                                Text('Too short! Long press!'),
                                duration: const Duration(seconds: 1, milliseconds: 500),
                                backgroundColor:
                                Colors.black,
                                behavior: SnackBarBehavior.floating,
                              );

                              Scaffold.of(context).showSnackBar(showSnackBar);
                            });
                          },
                          onLongPress: () {
                            setState(() {

                              if(check[3])
                                check[3] = false;
                              else
                                check[3] = true;

                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          //ITEM 5
          Column(
            children: [
              Container(
                height: 150,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Changing Percentage Value (Determinate)', textAlign: TextAlign.center,),
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 40,
                          //height: 30,
                          child: LinearProgressIndicator(
                            value: num1,
                            valueColor: check[4] ? AlwaysStoppedAnimation<Color> (Colors.amber) : AlwaysStoppedAnimation<Color> (Colors.blue),
                            minHeight: 30,

                          ),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {

                              if(num1+0.05 <= 1)
                                num1 += 0.1;
                              else
                                num1 = 0;

                              if((num1+0.05).toInt() == 1)
                                check[4] = true;
                              else
                                check[4] = false;

                              print(num1);

                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          Column(
            children: [
              Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Changing Size (Height)', textAlign: TextAlign.center,),
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 40,
                          //height: 100,
                          child: LinearProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color> (Colors.redAccent),
                            minHeight: definedHeight,
                          ),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              definedHeight = rng.nextInt(100).toDouble();
                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

          Column(
            children: [
              Container(
                  height: 150,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Changing Size (Width)', textAlign: TextAlign.center,),
                        SizedBox(
                          width: definedWidth,
                          //height: 30,
                          child: LinearProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color> (Colors.greenAccent),
                            minHeight: 30,
                          ),
                        ),
                        RaisedButton(
                          child: Text('Fun Button'),
                          onPressed: () {
                            setState(() {
                              definedWidth = rng.nextInt(300).toDouble();
                            });
                          },
                        ),
                      ],
                    ),
                  )),
            ],
          ),

        ],
        ),
    );
  }
}
