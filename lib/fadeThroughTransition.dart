import 'package:flutter/material.dart';
import 'package:animations/animations.dart';

class AppFT2 extends StatefulWidget {
  @override
  AppFT2State createState() => AppFT2State();
}

class AppFT2State extends State<AppFT2> {

  //stores the index value from BottomNavigatorBar
  int _selectedIndex = 0;
  //list of images which are tied to BottomNavigatorBar tabs
  final List<String> _pictures = ['images/BlueMountainRiver.jpg', 'images/forestRiver.jpg', 'images/SunsetMountain.jpg'];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Switcher Sample'),
        ),
        body: PageTransitionSwitcher(
          transitionBuilder: (
              Widget child,
              Animation<double> primaryAnimation,
              Animation<double> secondaryAnimation,
              ) {
            return FadeThroughTransition(
              child: child,
              animation: primaryAnimation,
              secondaryAnimation: secondaryAnimation,
            );
          },
          child: Container(
            key: ValueKey<int>(_selectedIndex),
            child: Image.asset(_pictures[_selectedIndex]),
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.photo),
              title: Text('One'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.photo),
              title: Text('Two'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.photo),
              title: Text('Three'),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber[800],
          onTap: (int index) {
            setState(() {
              _selectedIndex = index;
            });
          },
        ),
      ),

    );
  }
}